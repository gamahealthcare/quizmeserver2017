var csv = require("fast-csv");

var orm = require("orm");
const fslib = require('fs');
var stream = fslib.createReadStream("questions.csv");
var extratedDataFromCSV = null;

orm.connect("mysql://homestead:secret@localhost/quizme", function (err, db) {
	if (err) throw err;
	else{
		console.log("Connected to MYSQL");
		db.driver.execQuery("truncate question", function (err, data) {
			if (err) {
				console.log("Cannot truncate the table error: ", err);
			} else {
				console.log("Questions table truncated successfully!");
				console.log("Please wait...");
				QuestionDB = db.define('question', {
					id:      		{type: 'serial', key: true}, // the auto-incrementing primary key
					title:    	{type: 'text'},
					reference: 	{type: 'text'},
					answer1:  	{type: "text"},
					answer2: 		{type: 'text'},
					answer3:     	{type: 'text'},
					answer4: 		{type: 'text'},
					correct: 		{type: 'number'},
					image: 		{type: 'text'},
					category: 	{type: 'number'},
				});

				csv
				.fromStream(stream, {headers : ["id", "title", "reference", "answer1", "answer2", "answer3", "answer4", "correct", "image", "category"]})
				.on("data", function(data){
							if(data.id == "id") return;//skip first line
							questionForDB = {
								title: data.title,
								reference: data.reference,
								answer1: data.answer1,
								answer2: data.answer2,
								answer3: data.answer3,
								answer4: data.answer4,
								correct: data.correct,
								image: data.image,
								category: data.category,
							}

							QuestionDB.create(questionForDB, function(err, results) {
								if (err) {
									console.log("Cannot create the question into our DB: ");
									console.log(questionForDB.image);
									console.log(questionForDB.reference);
									console.log(questionForDB.category);
									console.log(err);
								}
							});
						})
				.on("end", function(){
					setTimeout(function(){
						console.log("All done! safe to close.");
					}, 2000)
				});
			}
		});
	}
});