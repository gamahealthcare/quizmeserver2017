"use strict"; 
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var orm = require("orm");
var db = null;
var PlayerDB = null;
var QuestionDB = null;
var GamesSessionDB = null;
var ScoreDB = null;
var mysql = null;
orm.connect("mysql://homestead:secret@localhost/quizme", function (err, db) {
	if (err) throw err;
	else{
		console.log("Connected to MYSQL");
		db = db;



		// db.driver.execQuery("select distinct category from question", function (err, data) {
		// 	if (err) {
		// 		console.log("cannot get the categories");
		// 	} else {
		// 		for (var i = data.length - 1; i >= 0; i--) {
		// 			var categoryID = data[i].category;
		// 			var getTwoQuestions = function(categoryID){
		// 				console.log(categoryID);
		// 				var query = "select * from question where category=" + categoryID;
		// 				console.log(query);
		// 				db.driver.execQuery(query, function (err, dataQuestionsFromCategory) {
		// 					if (err) {
		// 						console.log("cannot get the questions for category", categoryID, err);
		// 					} else {
		// 						//get two random ids
		// 						var firstRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
		// 						var secondRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
		// 						while(firstRandom == secondRandom){
		// 							firstRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
		// 							secondRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
		// 						}
		// 						console.log(dataQuestionsFromCategory[firstRandom], dataQuestionsFromCategory[secondRandom]);
		// 					}
		// 				}.bind(categoryID));
		// 			}
		// 			getTwoQuestions(categoryID);
		// 		};
		// 	}
		// });




		mysql = db.driver;
		PlayerDB = db.define('player', {
		  id:      		{type: 'serial', key: true}, // the auto-incrementing primary key
		  firstname:    {type: 'text'},
		  lastname: 	{type: 'text'},
		  email:  		{type: "text"},
		  organization: {type: 'text'},
		  top:     		{type: 'number'}

		}, {
		  methods : {
		    fullName: function() {
		      return this.firstname + ' ' + this.lastname;
		    }
		  }
		});

		QuestionDB = db.define('question', {
		  id:      		{type: 'serial', key: true}, // the auto-incrementing primary key
		  title:    	{type: 'text'},
		  reference: 	{type: 'text'},
		  answer1:  	{type: "text"},
		  answer2: 		{type: 'text'},
		  answer3:     	{type: 'text'},
		  answer4: 		{type: 'text'},
		  correct: 		{type: 'number'},
		  image: 		{type: 'text'},
		  category: 	{type: 'number'},
		});

		GamesSessionDB = db.define("gamessession", {
			id:      { type: 'serial', key: true},
			date:    { type: "date", time: true },
		});
		GamesSession.init();

		ScoreDB = db.define("score", {
			gamessession_id: 	{type: 'number', key: true},
			player_id: 			{type: 'number', key: true},
			score: 				{type: 'number'},
		});

	}
});

// var PlayerDB = require('./PlayerDB.js');

// PlayerDB.find({status:'active'}, function(err, results) {
//   // ...
// });
var ErrorType = {
	db_error_login: 100, //db error when login - client
	db_error_register: 101, //db error when register - client
	db_invalid_login: 102, //inexistent user for login, please register first - client
	no_players_for_game: 103, //no player available to start a new game - admin
	answer_is_not_for_the_current_question: 104, //the received answer is not for the current question and will be ignored -- client
	already_logged_in: 105, // the client is already logged in -- client
	error_max_logins: 106, // the maximum number of player that can login has been reached -- client
}



var DeviceType = {
	admin: "admin",
	client: "client",
	observer: "observer"
}
var Devices = {};
Devices[DeviceType.admin] = {},
Devices[DeviceType.client] = {};
Devices[DeviceType.observer] = {};




class Question {
	constructor(dbdata) {
		this.id = dbdata.id;
		this.title = dbdata.title;
		this.reference = dbdata.reference;
		this.answer1 = dbdata.answer1;
		this.answer2 = dbdata.answer2;
		this.answer3 = dbdata.answer3;
		this.answer4 = dbdata.answer4;
		this.correct = dbdata.correct;
		this.image = dbdata.image
		this.category = dbdata.category
	}

	setIndex(value) {
		this.index = value;
	}

	get data() {
		return {
			id: this.id,
			index: this.index,
			title: this.title,
			reference: this.reference,
			answer1: this.answer1,
			answer2: this.answer2,
			answer3: this.answer3,
			answer4: this.answer4,
			image: this.image,
			category: this.category,
		}
	}
}

class Game {


	constructor(){
		this.numberOfQestions = 12;
		this.questions = []; //will hold all the questions
		this.players = {};
		this.currentQuestionObj = null;
		this.lastQuestionResponses = [];
		
	}

	addPlayer(email, id) {
		if (typeof this.players[email] == "undefined") { //if the player wasn't in this game already to keep the score
			this.players[email] = {score:0, id: id}
		}
	}

	addPoints(email, points) {
		this.players[email].score += points;
	}

	getPoints(email) {
		return this.players[email].score;
	}

	init(callback) {
		mysql.execQuery("select distinct category from question order by category ASC", function (err, data) {
			if (err) {
				console.log("cannot get the categories");
			} else {
				var questionIndex = this.numberOfQestions;
				for (var i = data.length - 1; i >= 0; i--) {
					var categoryID = data[i].category;
					//var getTwoQuestions = function(categoryID){
						console.log(categoryID);
						var query = "select * from question where category=" + categoryID;
						console.log(query);
						mysql.execQuery(query, function (err, dataQuestionsFromCategory) {
							if (err) {
								console.log("cannot get the questions for category", categoryID, err);
							} else {
								//get two random ids
								var firstRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
								var secondRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
								while(firstRandom == secondRandom){
									firstRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
									secondRandom = parseInt(Math.random() * dataQuestionsFromCategory.length);
								}
								console.log(dataQuestionsFromCategory[firstRandom], dataQuestionsFromCategory[secondRandom]);
								var questionToStore1 = new Question(dataQuestionsFromCategory[firstRandom]);
								questionToStore1.setIndex(questionIndex);
								questionIndex--;
								this.questions.push(questionToStore1);
								var questionToStore2 = new Question(dataQuestionsFromCategory[secondRandom]);
								questionToStore2.setIndex(questionIndex);
								questionIndex--;
								this.questions.push(questionToStore2);

								if(this.questions.length >= this.numberOfQestions) {
									
									for (var uid in Client.all){
							    		if (Client.all.hasOwnProperty(uid)) {
							    			var client = Client.all[uid];
							         		if (client.isLoggedIn) {
							         			this.addPlayer(client.email, client.id);
							         		}
							         	}
							        }
									callback(false);
								}
								

							}
						}.bind(this));
					//}
					//getTwoQuestions(categoryID);
				};
			}
		}.bind(this));

		

		//get some questions from db
		// QuestionDB.all(function(err, dbQuestions){
		// 	if (err) {
		// 		console.log("tell admins that i cannot get the questions from DB");
		// 	} else {
		// 		console.log("I have a list of question: size: ", dbQuestions.length);
		// 		var questionIndex = this.numberOfQestions;
		// 		while(this.questions.length < this.numberOfQestions) {
		// 			var choosenQuestion = dbQuestions[parseInt(Math.random() * dbQuestions.length)];
		// 			var questionToStore = new Question(choosenQuestion);
		// 			questionToStore.setIndex(questionIndex);
		// 			questionIndex--;
		// 			this.questions.push(questionToStore);
		// 		}
		// 		console.log("NUMBER OF QUESTIONS IN THE GAME: ", this.questions.length);
		// 		for (var uid in Client.all){
		//     		if (Client.all.hasOwnProperty(uid)) {
		//     			var client = Client.all[uid];
		//          		if (client.isLoggedIn) {
		//          			this.addPlayer(client.email, client.id);
		//          		}
		//          	}
		//         }
		// 		callback(false);
		// 	}
		// }.bind(this));
	}

	static set questions(value) {
		
	}

	get currentQuestion() {
		return this.currentQuestionObj;
	}

	nextQuestion() {
		this.lastQuestionResponses = [];
		var nextQuestion = this.questions.pop();
		this.currentQuestionObj = nextQuestion;
		if (nextQuestion != null) {
			Device.emitNextQuestion(nextQuestion.data);
		} else {
			this.end();
		}
		// console.log("I am about to emit a question", this.questions.length);
	}

	static set inProgress(value){
		Game.isStarted = true;
	}

	static get inProgress(){
		if (typeof Game.isStarted == "undefined") {
			Game.isStarted = false;
		}
		return Game.isStarted;
	}

	addLastQuestionResponse(responseData) {
		if (responseData.questionID == this.currentQuestionObj.id) {
			this.lastQuestionResponses.push(responseData);
		};
	}

	get lastResponses() {
		return this.lastQuestionResponses;
	}

	setStatus(value)
	{
		console.log(2);
		Game.isStarted = value;
		Device.emitGameStatus();//game status changed
	}

	start() {
		console.log("START Game");
		console.log(1);
		this.setStatus(true);
		
		this.nextQuestion();
	}

	registerScore(playerID, score){
		ScoreDB.find({player_id: playerID, gamessession_id: GamesSession.sessionID}, function(err, data){
			if (err) {
				console.log("DB error when findind session score for player", err);
			} else {
				if (data.length == 0) {//doesent have a score in this session
					console.log("doesent have a score in this session");
					ScoreDB.create({
						player_id: playerID,
						gamessession_id: GamesSession.sessionID,
						score: score
					}, function(err, data){
						if (err) {
							console.log("DB error, cannot create score for this player", err);
						} else {
							console.log("NEW Score register");
							this.updatePlayerTopScore(playerID, score);
						}
					}.bind(this))
				} else{
					data = data[0];
					console.log("ABOUT TO COMPARE", data, data.score, score);
					if (data.score < score) { //i have a new score to register
						data.score = score;
						data.save(function(err){
							if (err) {
								console.log("i couldn't save the new higer score", err);
							} else {
								console.log("i saved the new score");
								//check the top score, this may have just changed
								this.updatePlayerTopScore(playerID, score);
							}

						}.bind(this));
					} else {
						console.log("the old score will be kept, the new score is not higher");
					}
				}
			}
		}.bind(this))
  		//check if exists and if so check is the value about to store is greater if not ignore
  		//save the new score if this is the case
	}

	updatePlayerTopScore(playerID, score) {
		PlayerDB.find({id: playerID}, function(err, playerData){
			if (err) {
				console.log("DB error when finding player");
			} else {
				if (playerData.length > 0) {
					//i have the player
					playerData = playerData[0];
					if (playerData.top < score) {
						console.log("I have a new top score for this player");
						playerData.top = score;
						playerData.save(function(err){
							if (err) {
								console.log("DB error couldn't save the new top score for player")
							} else {
								console.log("the new top score for this player has been updated");
							}
						});
					} else {
						console.log("the old top score for this player will be kept");
					}
				} else {
					console.log("Cannot find the player with id: ", playerID);
				}
			}
		})
	}

	end() {
		console.log("GAME END");
		Game.isStarted = false;
		Device.emitGameEnded();
		for (var email in this.players){
		    if (this.players.hasOwnProperty(email)) {
		        var playerInGame = this.players[email];
		        console.log("ABOUT TO REGISTER SCORE FOR: ", playerInGame, playerInGame.id, playerInGame.score);
				this.registerScore(playerInGame.id, playerInGame.score);
		    }
		}
		
	}
}

class GamesSession {
	static startNewGame() {
		GamesSession.gameInProgress = new Game();
		GamesSession.gameInProgress.init(function(err){
  				if(err) {
  					console.log("Game wasn't initialized");
  				} else {
  					console.log("START GamesSession");
  					GamesSession.gameInProgress.start();
  				}
  			})
	}

	static get maxNumberOfLogins()
	{
		return 4;
	}

	static set sessionID(value) {
		GamesSession.id = value;
	}

	static get sessionID() {
		return GamesSession.id;
	}

	static get sessionDate() {
		return GamesSession.date;
	}

	static createNew() {
		GamesSessionDB.create({date: new Date()}, function(err, data){
			if (err) {
				console.log("I cannot create the session", err);
			} else {
				console.log("Current session id that i created:", data.id);
				GamesSession.id = data.id;
		    	GamesSession.date = data.date;
			}
		})
	}

	static init() {

		mysql.execQuery("select * from gamessession where id=(select max(id) from gamessession)", function (err, data) {
			if (err) {
  				console.log("Cannot get the current session id");
  			} else {
		    	if (data.length == 0) {
		    		console.log("There is no session available i will create one");
		    		GamesSession.createNew();
		    	} else {
		    		data = data[0];
		    		console.log("Current session id:", data.id);
		    		GamesSession.id = data.id;
		    		GamesSession.date = data.date;
		    	}
  			}
		});
	}

	static generatePlayerIndex() {
		for (var playerLoginIndex = 0; GamesSession.maxNumberOfLogins > playerLoginIndex; playerLoginIndex++) {
			var indexUsed = false;
			for (var uid in Client.all){
			    if (Client.all.hasOwnProperty(uid)) {
			         var client = Client.all[uid];
			         if (client.isLoggedIn && (client.index == playerLoginIndex)) {
			         	indexUsed = true;
			         	break;
			         };
			    }
			}
			if(! indexUsed) {
				return playerLoginIndex;
			}
		};
	}

		//get the highest session id
  // 		GamesSessionDB.aggregate().max("id").get(function (err, highestID) {
  // 			if (err) {
  // 				console.log("Cannot get the current session id");
  // 			} else {
		//     	if (highestID == null) {
		//     		console.log("There is no session available i will create one");
		//     		GamesSessionDB.create({date: new Date()}, function(err, data){
		//     			if (err) {
		//     				console.log("I cannot create the session", err);
		//     			} else {
		//     				console.log("Current session id that i created:", data.id);
		//     				GamesSession.sessionID = data.id;
		//     			}
		//     		})
		//     	} else {
		//     		console.log("Current session id:", highestID);
		//     		GamesSession.sessionID = highestID;
		//     	}
  // 			}
		// });
	// }

	static storeResponse(responseData) {
		GamesSession.gameInProgress.addLastQuestionResponse(responseData);
	}

	static get lastResponses() {
		return GamesSession.gameInProgress.lastResponses;
	}

	static get currentGame(){
		return GamesSession.gameInProgress;
	}

	static get hasGameProgress(){
		return Game.inProgress;
	}

	static get currentLoginsInGame() {
		var logins = [];//TODO make this static and mantain it at logins and disconnects?
		for (var uid in Client.all){
		    if (Client.all.hasOwnProperty(uid)) {
		         var clientToInform = Client.all[uid];
		         if (clientToInform.isLoggedIn) {//let the just logget in player about the other logins
		         	logins.push(clientToInform.data);
		         };
		    }
		}
		return logins;
	}

	static get lastQuestion() {
		return GamesSession.gameInProgress.currentQuestion.data;
	}

	static get dataForAdminReconnect() {
		return {
			"logins": GamesSession.currentLoginsInGame,
			"responses": GamesSession.lastResponses,
			"question": GamesSession.lastQuestion,
		}
		
	}

	static endCurrentGame(){
		console.log("endCurrentGame", GamesSession.hasGameProgress);
		if (Game.inProgress) {
			GamesSession.gameInProgress.end();
		}
	}
}

class Device {

	constructor(socket, data, deviceType) {
		// console.log("DEVICE CONTRUCTOR");
		// console.log("device type received: ", deviceType);
		this.deviceType = deviceType;
		this.uid = data.uid;
		Admin.emitDeviceConnected(this);
		this.socket = socket;
		this.register();
	}

	register () {
		//console.log("register the device ", this.deviceType, this.uid);
		Devices[this.deviceType][this.uid] = this;
	}

	get isClient() {
		return true;
	}

  	disconnect() {
  		console.log("device disconnected",this.deviceType, this.uid)
  		if (this.isClient) {
  			io.emit("client-disconnected", this.uid);
  		} else {
  			delete Devices[this.deviceType][this.uid];
  			this.emitAdminOffline();
  		}
  		delete Devices[this.deviceType][this.uid];
  		console.log("CLIENTS REMAINING: ", Object.keys(Client.all).length);
  		if (Client.countLogins() == 0) {
  			GamesSession.endCurrentGame();
  		};
  	}

  	static emitNextQuestion(questionData) {
  		questionData.timestamp = new Date();
  		io.emit("next-question", questionData);
  	}

  	//let all admins that the client logged in
  	emitPlayerLogin(data) {
  		//io.emit("client-logged-in", data); return;
  		for (var uid in Client.all){
		    if (Client.all.hasOwnProperty(uid)) {
		         var clientToInform = Client.all[uid];
		         clientToInform.sendClientOnline(data);
		         if (clientToInform.isLoggedIn) {//let the just logget in player about the other logins
		         	this.sendClientOnline(clientToInform.data);
		         };
		    }
		}
		for (var uid in Admin.all){
		    if (Admin.all.hasOwnProperty(uid)) {
		    	console.log("Notify admin");
		         Admin.all[uid].sendClientOnline(data);
		    }
		}
  		
  	}

  	sendAdminStatus() {
  		this.socket.emit("admin-is-online", Admin.isOnline());
  	}

  	emitAdminOffline() {
  		if (! Admin.isOnline()) {
  			io.emit("admin-is-online", false);
  		}
  	}

  	sendClientOnline(data) {
		this.socket.emit("client-logged-in", data);
	}

	static emitGameEnded() {
		io.emit("game-in-progress", false);
	}

	static emitQuestionResults(data) {
		io.emit("question-results", data);
	}

	sendGameStatus() {
  		this.socket.emit("game-in-progress", GamesSession.hasGameProgress);
  	}
  	static emitGameStatus() //true,false
  	{
  		console.log(3);
  		io.emit("game-in-progress", GamesSession.hasGameProgress);
  	}
}

class Observer extends Device{
	constructor(socket, data) {
		//console.log("CLIENT CONTRUCTOR");
		super(socket, data, DeviceType.observer);
	}

	get isObserver(){
		return true;
	}

	completeRegistration(){
		console.log("Observer online");
	}

	static get all(){
  		return Devices[DeviceType.observer];
  	}
}

class Client extends Device{
	constructor(socket, data) {
		//console.log("CLIENT CONTRUCTOR");
		super(socket, data, DeviceType.client);
	}

	get isClient() {
		return true;
	}

  	emitConnect(){
  		console.log("I am client and i will let admins to know that i am connected");
  	}

  	completeRegistration(){
  		this.emitConnect();
  		//if the device is client and i have admin online i will let the client know
		//if (Admin.isOnline()) {
			//console.log("I HVE ADMIN ONLINE");
			// device = this;
			//now the client can login
			this.socket.on('login-client', function(data){this.loginEvent(data)}.bind(this));
			//now the client can register
			this.socket.on('register-client', function(data){this.registerEvent(data)}.bind(this));
			this.sendAdminStatus();
			this.sendGameStatus();
		//} else {
			//console.log("NOOOOOOOOOOOOOO admin online")
		//}
  	}

  	addPoints(points) {
  		console.log("added points: " + this.email, points);
  		GamesSession.currentGame.addPoints(this.email, points);
  	}

  	get score() {
  		console.log("score: " + this.email, GamesSession.currentGame.getPoints(this.email));
  		return GamesSession.currentGame.getPoints(this.email);
  	}

  	checkAnswerEvent(data){

  		//get the current question
  		console.log("checking the answer for" + this.uid, data);
  		var question = GamesSession.currentGame.currentQuestion;
  		if (question.id == data.questionID) {
  			//i have the answer for the current question
  			//check if the current question is the same from the answer
  			//check if the answer is right and compute the current's game score for this player timeLeftPercent * category *10
  			if (question.correct == data.answer) {
  				//i have a valid answer
  				//var answerScore = data.time * question.category / 14;
  				var answerScore = data.time * 0.25;
  				this.addPoints(answerScore);
  			}
  		} else {
  			this.sendError(ErrorType.answer_is_not_for_the_current_question);
  		}


  		//feedback about the question to this client
  		//feedback about this question to the admins
  		var responseData = { "questionID": data.questionID, "score": Math.round(this.score), "uid": this.uid, "time": data.time, "questionIndex": question.index}
  		Device.emitQuestionResults(responseData);
  		GamesSession.storeResponse(responseData);
  	}

  	get data() {
  		return {
  			"email": this.email, 
  			"firstname": this.firstname, 
  			"lastname": this.lastname, 
  			"organization": this.organization,
  			"uid": this.uid,
  			"index": this.index,
  		}
  	}

  	sendLoginSuccess() {
  		this.socket.emit("login-success", null);
  		this.emitPlayerLogin(this.data);
  		if (GamesSession.hasGameProgress) {//TODO do not add in game if this is started and wasn't available since the begin
  			GamesSession.currentGame.addPlayer(this.email, this.id);
  			this.socket.emit("next-question", GamesSession.currentGame.currentQuestion.data);
  		};
  	}

  	get isLoggedIn() {
  		if (typeof this.loggedIn == "undefined") {
  			this.loggedIn = false;
  		};
  		return this.loggedIn;
  	}

  	static clientAlreadyLoggedIn(email) {
  		for (var uid in Client.all){
		    if (Client.all.hasOwnProperty(uid)) {
		        var client = Client.all[uid];
		        console.log(client.isLoggedIn,Client.all[uid].email,email);
		        if (client.isLoggedIn && (Client.all[uid].email == email)) {
		        	return true;
		        }
		    }
		}
		return false;
  	}

  	loginEvent(data) {
  		if (Client.clientAlreadyLoggedIn(data.email)) {
  			this.sendError(ErrorType.already_logged_in);
  			return;
  		}

  		if (Client.countLogins >= GamesSession.maxNumberOfLogins) {
  			this.sendError(ErrorType.error_max_logins);
  		};

  		console.log("client logged with: ", data);
  		PlayerDB.find({email:data.email}, function(err, results) {
  			if (err) {
  				console.log("DB ERROR:", err);
  				this.sendError(ErrorType.db_error_login);
  			} else {
  				if (results.length == 0) {
  					//i do not have this player for login
  					this.sendError(ErrorType.db_invalid_login);
  				} else {
  					var clientDbData = results[0];
  					this.id = clientDbData.id;
  					this.email = clientDbData.email;
  					this.firstname = clientDbData.firstname;
  					this.lastname = clientDbData.lastname;
  					this.organization = clientDbData.organization;
  					this.index = GamesSession.generatePlayerIndex();//save the index of the client
  					this.sendLoginSuccess();
  					this.socket.on("question-answer", function(data){this.checkAnswerEvent(data)}.bind(this))
  					this.loggedIn = true;//--NOT AVAIL --> important at the bottom so he wont appear as logged in
  				}
  			}
  		}.bind(this));
  	}

  	static countLogins()
  	{
  		var index = 0;
  		for (var uid in Client.all){
		    if (Client.all.hasOwnProperty(uid)) {
		         // alert("Key is " + k + ", value is" + target[k]);
		        if(Client.all[uid].isLoggedIn) {
		        	index++;
		        }
		    }
		}
		return index;
  	}

  	sendError(value) {
  		this.socket.emit("client-error", {code: value});
  	}

  	registerEvent(data) {
  		console.log("client register with: ", data);
  		PlayerDB.find({email:data.email}, function(err, results) {
  			if (err) {
  				console.log("DB ERROR:", err);
  			} else {
  				//i do not have this player if 0 so i will register
				if (results.length == 0) {
					data.top = 0;
					PlayerDB.create(data, function(err, results) {
						if (err) {
							console.log("Cannot create the player into the DB");
							console.log(err);
						} else {
							console.log("Player registered to DB");
							//continue with his login:
							this.loginEvent(data);
						}
					}.bind(this));
				} else {
					//i have this player so i will continue as login
					this.loginEvent(data);
				}
  			}
		}.bind(this));
  	}

  	static get all(){
  		return Devices[DeviceType.client];
  	}

  	static get isOnline() {
  		// console.log("Client.all.length", Client.all, Client.all.length);
  		return (Object.keys(Client.all).length > 0) ? true : false;
  	}
}

class Admin extends Device{

	constructor(socket, data) {
		console.log("ADMIN CONTRUCTOR");
		super(socket, data, DeviceType.admin);
	}

  	emitConnect(){
  		console.log("I am admin and i will let client to know that they can login");
  		for (var uid in Client.all){
		    if (Client.all.hasOwnProperty(uid)) {
		         // alert("Key is " + k + ", value is" + target[k]);
		         Client.all[uid].sendAdminStatus();
		    }
		}
  	}

  	requestNewSession() {
  		GamesSession.createNew();
  	}

  	completeRegistration() {
  		//tell the devices that admin is now online
  		this.emitConnect();
  		this.sendGameStatus();
  		//listen for this admin when sarts a game
  		this.socket.on('new-game', function(){this.requestNewGame()}.bind(this));
  		this.socket.on('next-question', function(){this.requestNextQuestion()}.bind(this));
  		this.socket.on('end-game', function(){this.requestEndGame()}.bind(this));
  		this.socket.on('new-session', function(){this.requestNewSession()}.bind(this));
  		if (GamesSession.hasGameProgress) {
  			this.socket.emit("game-in-progress-data", GamesSession.dataForAdminReconnect);
  		} else {
  			//send to the admin the current logins
  			for (var uid in Client.all){
			    if (Client.all.hasOwnProperty(uid)) {
			        var client = Client.all[uid];
			        if (client.isLoggedIn) {
  						this.socket.emit("client-logged-in", client.data);
			        } else {
						this.socket.emit("device-connected", uid, client.deviceType);
					}
			    }
			}
  		}
  	}

	static emitDeviceConnected(device){
		for (var uid in Admin.all){
			var admin = Admin.all[uid];
			admin.socket.emit("device-connected", device.uid, device.deviceType);
		}
	}

  	sendError(value) {
  		this.socket.emit("admin-error", {code: value});
  	}

  	requestNewGame() {
  		console.log("Client.isOnline", Client.isOnline);
  		if (! Client.isOnline) {
  			this.sendError(ErrorType.no_players_for_game);
  		} else {
  			console.log("Admin requested to start a new game");
  			GamesSession.startNewGame();
  		}
  	}

	get isClient() {
		return false;
	}

  	requestEndGame() {
  		console.log("Admin requested end-game");
  		GamesSession.endCurrentGame();
  	}

  	requestNextQuestion() {
  		//if i have a game in progress
  		console.log("Admin requested to go to the next question");
  		GamesSession.currentGame.nextQuestion();
  	}

  	static get all(){
  		return Devices[DeviceType.admin];
  	}

  	static isOnline() {
  		return (Object.keys(Devices[DeviceType.admin]).length > 0) ? true : false;
  	}
}



app.get('/', function(req, res){
  	res.sendfile('index.html');
});

app.get('/score/top', function(req, response){
	mysql.execQuery("SELECT firstname, lastname,email, top as score FROM player order by top DESC", function (err, data) {
		if (! err) {
			response.send({success: 1, data: data});
		} else {
			response.send({success: 0});			
		}
	});
});


app.get('/questions', function(req, response){
	mysql.execQuery("SELECT * FROM question order by id", function (err, data) {
		if (! err) {
			response.send({success: 1, data: data});
		} else {
			response.send({success: 0});			
		}
	});
});

app.get('/score/tops', function(req, response){
	mysql.execQuery("SELECT email, top as score FROM player order by top DESC", function (err, data) {
		if (! err) {
			response.send({success: 1, data: data});
		} else {
			response.send({success: 0});			
		}
	});
});

app.get('/score/session', function(req, response){
	mysql.execQuery("SELECT firstname, lastname,email, score as score FROM player"
     	+ " join score on player.id = score.player_id"
		+ " where score.gamessession_id = " + GamesSession.sessionID
		+ " order by score DESC", function (err, data) {
		if (! err) {
			response.send({
				success: 1, 
				data: {
					players: data,
					name: GamesSession.sessionDate,
					id: GamesSession.sessionID,
				}
			});
		} else {
			console.log(err);
			response.send({success: 0});			
		}
	});
});

io.on('connection', function(socket){
  	//console.log('a user connected');
	socket.on('register-device', function(data){
		//console.log("register device event:");
		//console.log("new device arived: ", data);
		var device = null;
		switch(data.role){
			case "admin":
				device = new Admin(socket, data);
				break;
			case "client":
				device = new Client(socket, data);
				break;
			default:
				device = new Observer(socket, data);
				break;

		}
		device.completeRegistration();
		//console.log("=========ALL GOOD");
		

		socket.on('disconnect', function(){device.disconnect()});
	});
	// //register other events
	// return;
});

app.listen(80, function () {
  console.log('API ready!');
});

http.listen(8889, function(){
  console.log('WebSocket ready!');
});
