var socket = require('socket.io-client')('http://localhost:8889');

var playersCount = 0;
var gameInProgress = true;

socket.on('connect', function(){
	console.log("I am connected");
	socket.emit('register-device', { uid: 'adminuid' + Math.floor((Math.random() * 1000) + 1), role: "admin" });
	socket.emit('new-session', null);
});

socket.on("client-logged-in", function(data){
	console.log("Some player logged in:", data);
	playersCount++;
	if ((playersCount == 2) && (! gameInProgress) ) {//will start the game with two clients and there is no game in progress
		//console.log("i can start the game:");
		socket.emit('new-game', null);//this will trigger next-question for all devices
	}
})

var askNextQuestion = function() {
	//if the game didn't end i will ask the next question
	if (gameInProgress) { //if the game didn't ended
		//delay next question 2 seconds:
		setTimeout(function(){
			socket.emit('next-question', null);
		}, 10000);
	}
}

var responseCount = 0;
var currentQuestionID = null;
socket.on("question-results", function(data){
	//console.log("Results of a question:", data);

	//if the response is for the current question i will take it into consideration
	if (currentQuestionID == data.questionID) {
		responseCount++;
	};

	//console.log("Why not next question? ", responseCount, playersCount, gameInProgress);
	//if all players responded, i will ask for the next question
	if (responseCount >= playersCount) {
		//reset the question and response counts:
		responseCount = 0;
		currentQuestionID = null;

		askNextQuestion();
		
	};
})

socket.on("game-in-progress-data", function(data){
	//console.log("It seems that i already have a game in progress, here is the data: ", data);
	currentQuestionID = data.question.id;
	responseCount = data.responses.length;
	playersCount = data.logins.length;
	/**
		If the responses are not the same as number of players, take care to expect that response
		because the admin may reconnect before that client responded
	*/
	askNextQuestion();
})

socket.on('game-in-progress',function(status){
	console.log("Game is in progress? ", status);
	gameInProgress = status;
	// if (! status) {
	// 	console.log("NO GAME AVAILABLE, START THE GAME IN 10sec...");
	// 	setTimeout(function(){
	// 		if (! gameInProgress) {
	// 			socket.emit('new-game', null);
	// 			console.log("I started the game!");
	// 		} else {
	// 			console.log("Game was already in progress, i couldn't started");
	// 		}
	// 	}, 10000);
	// };
});

socket.on("client-disconnected", function(uid){
	//console.log("Some player disconnected:", uid);
	playersCount--;
})

socket.on("admin-error", function(data){
	//console.log("ERROR RECEIVED: ", data);
	//no_players_for_game: 103, //no player available to start a new game - admin
})

socket.on("next-question", function(data){
	gameInProgress = true;//game started
	currentQuestionID = data.id;
	//console.log("I received the next question withid: " + currentQuestionID, data);
});

socket.on('disconnect', function(){
	console.log("I am disconnected");
});

// setTimeout(function(){
// 	socket.emit('end-game', null);
// }, 15000)