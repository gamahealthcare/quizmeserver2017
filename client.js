var socket = require('socket.io-client')('http://localhost:8889');

var playerUnique = Math.floor((Math.random() * 1000000) + 1);
var gamesInprogress =false;
var adminOnline = false;
var loggedIn = false;
socket.on('connect', function(){
	// console.log("I am connected");
	socket.emit('register-device', { uid: 'uid_' + playerUnique, role: "client" });
});
var registerClient = function(){
	if (adminOnline && ! gamesInprogress && ! loggedIn) {
		// console.log("send register");
		socket.emit('register-client', { 
			email: "email_" + playerUnique + "@email.dom", 
			firstname:"F_" + playerUnique,
			lastname: "L_" + playerUnique,
			organization: "O_" + playerUnique,
		});

		// socket.emit('login-client', { 
		// 	email: "email_324559@email.dom", 
		// });
		
	}
}



socket.on('admin-is-online', function(status){
	//console.log("The admin is online? ", status);
	adminOnline = status;
	registerClient();
});

socket.on("client-logged-in", function(data){
	console.log("Some player logged in:", data);
})

socket.on("client-disconnected", function(uid){
	//console.log("Some player disconnected:", uid);
})

socket.on('game-in-progress',function(status){
	console.log("Game is in progress? ", status);
	gamesInprogress = status;
	// registerClient();
	});


// socket.on("question-results", function(data){
// 	console.log("question results data: ", data);
// })

socket.on("question-results", function(data){
	//console.log("Results of a question:", data);
})

socket.on("login-success", function(){
	//console.log("I am logged in! Hurray!");
	loggedIn = true;
})

socket.on("client-error", function(data){
	//console.log("ERROR RECEIVED: ", data);
	// db_error_login: 100, //db error when login - client
	// db_error_register: 101, //db error when register - client
	// db_invalid_login: 102, //inexistent user for login, please register first - client

	// answer_is_not_for_the_current_question: 104, //the received answer is not for the current question and will be ignored -- client
	// already_logged_in: 105, // the client is already logged in -- client
})

socket.on("next-question", function(data){
	//console.log("I received the next question", data);
	socket.emit("question-answer", {
		questionID: data.id, 
		answer: parseInt(Math.random() * 4) + 1, 
		time: parseInt(Math.random() * 100)
	})
});

socket.on('disconnect', function(){
	console.log("I am disconnected");
	loggedIn = false;
});