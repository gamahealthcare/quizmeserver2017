var socket = require('socket.io-client')('http://localhost:8889');

var playerUnique = Math.floor((Math.random() * 1000000) + 1);
var gamesInprogress =false;
var adminOnline = false;
var loggedIn = false;
socket.on('connect', function(){
	console.log("I am connected");
	socket.emit('register-device', { uid: 'uid_' + playerUnique, role: "observer" });
});

socket.on('game-in-progress',function(status){
	console.log("Observer: Game is in progress? ", status);
	gamesInprogress = status;
	// registerClient();
});